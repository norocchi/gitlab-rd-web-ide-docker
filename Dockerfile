FROM ubuntu:22.04

WORKDIR gitlab-rd-web-ide

COPY ./scripts scripts
COPY ./nginx.conf.template nginx.conf.template
COPY ./vscode-reh vscode-reh

ARG USERNAME=tanuki
ARG USER_UID=1000
ARG USER_GID=$USER_UID

RUN apt-get update \
  && apt-get install -y nginx git sudo \
  && rm -rf /var/lib/apt/lists/* \
  && groupadd --gid $USER_GID $USERNAME \
  && useradd --uid $USER_UID --gid $USER_GID -m $USERNAME \
  && usermod -G sudo $USERNAME

ENTRYPOINT ["./scripts/start.sh"]
