#!/bin/sh

export ROOT_DIR="$( cd "$( dirname "$0" )" && cd .. && pwd )"
export VSCODE_REH_DIR="vscode-reh"
export VSCODE_REH_TOKEN_FILE="${ROOT_DIR}/TOKEN"
